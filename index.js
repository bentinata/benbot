'use strict';

const flow = require(`lodash/fp/flow`);
const split = require(`lodash/fp/split`);
const get = require(`lodash/fp/get`);
const replace = require(`lodash/fp/replace`);
const chalk = require(`chalk`);

const log = console.log;

global.Promise = require(`bluebird`);
global.console.log = (...args) => {
  log(...args, chalk.gray(flow(
    split(`\n`),
    get(2),
    replace(`)`, ``),
    s => s.slice(s.indexOf(__dirname) + __dirname.length + 1)
  )((new Error()).stack)));
};
