'use strict';

const botkit = require(`botkit`);
const sample = require(`lodash/fp/sample`);
const chat = botkit.slackbot({
  debug: false,
});

const bentinata = `U02JRT89J`;

chat
  .spawn({ token: process.env.TOKEN })
  .startRTM();

chat.on(`direct_message`, (bot, message) => {
  if (message.user === bentinata) {
    bot.reply(message, `Ok.`);
  } else {
    bot.reply(message, `You are not <@${bentinata}>.`);
  }
});

chat.on(`mention`, (bot, message) => {
  if (message.user === bentinata) {
    const response = sample([
      `I don't know.`,
      `Sure!`,
      `Maybe...`,
      `No.`,
      `It depends...`,
    ]);
    bot.reply(message, response);
  }
});
