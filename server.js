'use strict';

const { token, owner, listen } = require(`./config`);
const http = require(`http`);
const fs = require(`fs`);
const koa = require(`koa`);
const app = new koa();
const botkit = require(`botkit`);
const controller = botkit.slackbot();
const isString = require(`lodash/fp/isString`);
const { name, version } = require(`./package.json`);
const exit = require(`./exit`);

const server = http.createServer(app.callback());

server.listen(listen, () => {
  const address = server.address();

  if (isString(address)) {
    console.log(`${name}-${version} ${address}`);
    exit(callback => fs.unlink(address, callback));
  } else {
    console.log(`${name}-${version} ${address.address}${address.port}`);
  }
});

controller
  .spawn({ token })
  .startRTM();

controller.on(`direct_message`, (bot, message) => {
  if (message.user === owner) {
    bot.reply(message, `Ok.`);
  } else {
    bot.reply(message, `You are not <@${owner}>.`);
  }
});
