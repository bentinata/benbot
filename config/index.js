'use strict';

const merge = require(`lodash/fp/merge`);
const some = require(`lodash/fp/some`);
const findKey = require(`lodash/fp/findKey`);
const isUndefined = require(`lodash/fp/isUndefined`);

// Default values, might be default database or else.
const def = {
  owner: `U02JRT89J`,
  listen: 0,
};

const specific = require(`./${process.env.CONFIG || ``}`);

// Should've put process.env.* here.
const override = {
  token: process.env.TOKEN,
  owner: process.env.OWNER,
  listen: process.env.LISTEN,
};

const config = merge(merge(def, specific), override);

if (some(isUndefined, config)) {
  const missing = findKey(isUndefined, config);
  throw new Error(`Bad config. Missing ${missing}.`);
}

module.exports = config;
