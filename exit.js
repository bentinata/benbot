'use strict';

const parallel = require(`async/parallel`);
const tasks = [];

process.once(`SIGINT`, () => {
  process.kill(process.pid, `SIGUSR2`);
});

process.once(`SIGUSR2`, () => {
  parallel(tasks, (err) => {
    if (err) {
      console.log(err);
    }
    process.kill(process.pid, `SIGUSR2`);
  });
});

module.exports = (fn) => {
  console.log(`Added handler.`);
  tasks.push(fn);
};
