'use strict';

const config = require(`./config`);
const discord = require(`discord.js`);
const code = require(`./lib/code`);

const client = new discord.Client();

client.on(`message`, async ({ cleanContent: content, channel }) => {
  if (content.slice(0, 1) === `!`) {
    const raw = content.slice(1);

    const extracted = code.extract(raw);
    const stdout = await code.run(extracted);

    return channel.send(code.toCode(stdout));
  }

  return false;
});

client.on(`error`, (err) => {
  console.log(err);
});

client.on(`ready`, () => {
  const name = client.user.username;

  console.log(`${name} connected.`);
});

client.login(config.token);
