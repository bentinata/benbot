'use strict';

const koa = require(`koa`);
const app = new koa();

app.use(({ response }) => {
  response.body = `ok`;
});

module.exports = app;
