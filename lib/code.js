'use strict';


const util = require('util');
const childProcess = require('child_process');
const fs = require('fs');
const shortid = require('shortid');

const exec = util.promisify(childProcess.exec);
const writeFile = util.promisify(fs.writeFile);
const unlink = util.promisify(fs.unlink);
const MARKER = '```';

const extract = (raw) => {
  const split = raw.split('\n');
  const [start, end] = split.filter(line => line.includes(MARKER));
  const lang = start.replace(MARKER, '');

  const code = split
    .slice(split.indexOf(start) + 1, split.indexOf(end))
    .join('\n');

  if (!lang) {
    return new Error('No language defined.');
  }

  return {
    code,
    lang,
  };
};

const toCode = s => `${MARKER}\n${s}\n${MARKER}`;

const run = async ({ code, lang }) => {
  const interpreters = [
    {
      lang: ['js', 'node', 'javascript'],
      bin: 'node',
    },
    {
      lang: ['sh', 'bash'],
      bin: 'bash',
    },
    {
      lang: ['c'],
      bin: 'tcc -run',
    },
  ];

  const interpreter = interpreters.find(intr => intr.lang.includes(lang));

  if (!interpreter) {
    return new Error('Not supported.');
  }

  const filename = `tmp-${shortid.generate()}.${interpreter.lang[0]}`;
  await writeFile(filename, code, 'utf8');
  const res = await exec(`${interpreter.bin} ${filename}`);
  console.log(res);
  await unlink(filename);

  return res.stdout;
};

module.exports = {
  MARKER,
  extract,
  toCode,
  run,
};
